import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../../service/profilService';
import {Profil} from '../../models/profil';

@Component({
  selector: 'app-profils',
  templateUrl: './profils.component.html',
  styleUrls: ['./profils.component.css']
})
export class ProfilsComponent implements OnInit {

  profiles: Profil[];


  constructor(private profileService: ProfileService) { }

  ngOnInit() {
    this.profileService.getProfils().subscribe(result => {
      result.map(profils => profils.profils = this.profiles);
    });
    console.log(this.profiles);
  }
}
