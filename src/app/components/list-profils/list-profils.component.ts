import { Component, OnInit } from '@angular/core';
import {Profil} from '../../models/profil';
import {ProfileService} from '../../service/profilService';
import {Profils} from '../../models/profils';

@Component({
  selector: 'app-list-profils',
  templateUrl: './list-profils.component.html',
  styleUrls: ['./list-profils.component.css']
})
export class ListProfilsComponent implements OnInit {

  constructor(private profileService: ProfileService) {
  }

  ngOnInit() {
  }
}
