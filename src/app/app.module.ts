import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { ProfilComponent } from './components/profil/profil.component';
import { ListProfilsComponent } from './components/list-profils/list-profils.component';
import {ProfileService} from './service/profilService';
import { ProfilsComponent } from './components/profils/profils.component';

const appRoutes: Routes = [
  { path: 'profils', component: ListProfilsComponent },
  { path: 'profil', component: ProfilComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ProfilComponent,
    ListProfilsComponent,
    ProfilsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    FormsModule
  ],
  providers: [
    ProfileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
