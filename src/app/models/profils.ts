import {Profil} from './profil';

export class Profils {
  profils: Profil[];
  nbProfils: number;

  constructor(profils: Profil[], nbProfils: number) {
    this.profils = profils;
    this.nbProfils = nbProfils;
  }
}
