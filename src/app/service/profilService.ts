import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Profil} from '../models/profil';
import {Profils} from '../models/profils';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient) {
  }

  getProfils(): Observable<Profils[]> {
    return this.http.get<Profils[]>('http://localhost:8088/zinder/profils');
  }

}
